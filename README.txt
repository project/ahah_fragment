-------------------------------------------------------------------------------
AHAH Fragment for Drupal 5.x
  by Ronan Dowling, Gorton Studios - ronan (at) gortonstudios (dot) com	
-------------------------------------------------------------------------------
 
AHAH Fragment is a module which returns only the requested fragment from the
requested page. This allows users to create callbacks for AHAH functionality with
little or no code.

When the module is enabled, any fragment of a page can be extracted by simply
adding a get parameter to the url describing the fragment desired. Fragments can
be described using standard css selectors like 'div.node' or 
'div > span.class + #someid'

To get, for example, only the h2 elements with the class 'title' of a page use
  node/1234?ahah_fragment=h2.title
Or to get an element with the id 'video' from the page videos/1 use:
  videos/1?ahah_fragment=%23video
  (thats div#video url encoded)

If multiple fragments match the selector they will all be returned one after the
other.

The module also integrates with thickbox to provide a simple way to create a
callback for thickbox windows. To put the above mentioned div in a thickbox 
simply create a link with the class 'thickbox-ahah-fragment' the fragment id in
the url eg:
  <a href="videos/1#video" class="thickbox-ahah-fragment">watch the video</a>

The element on that page with the id 'video' will be fetched and shown in a 
thickbox popup.

-------------------------------------------------------------------------------
Installation
------------
Go to administer -> site building -> modules and enable the module.

That's it pretty much, there is no configuration.

-------------------------------------------------------------------------------
Known Issues
------------
This is not the most efficient way to generate a fragment for an AHAH callback 
since in many cases the entire page must be rendered before the element required
is extracted. If you're doing any serious AHAH work you may be better writing
a custom callback. This module is a really quick way to get started though.

It does not use the page cache so fragments are generated anew each time.

Thickbox integration js is added to every page whether it's needed or not

-------------------------------------------------------------------------------
TODO
----
* Better caching
-------------------