
if (Drupal.jsEnabled) {
  //on page load call ahah_fragment_tb_init
  $(document).ready(function(){
      ahah_fragment_tb_init('a.thickbox-ahah-fragment, area.thickbox-ahah-fragment, input.thickbox-ahah-fragment');
  });
}

// add thickbox with ahah-fragment ready urls to elements that have a class of .thickbox-ahah-fragment
function ahah_fragment_tb_init( domChunk ){
  $(domChunk).click(function(){
    var t = this.title || this.name || null;
    var a = this.href || this.alt;
    var g = this.rel || false;
    a = ahah_fragment_href(a, 'html');
    tb_show(t,a,g);
    this.blur();
    return false;
  });
}

function ahah_fragment_href( url, format ) {
  format = format || 'html';
  if( url.indexOf("#")!==-1 ){ //find the fragment
    var query = "";
    var fragment = url.substr( url.indexOf("#") + 1 );
    url = url.substr(0, url.indexOf("#"));
    
    if( url.indexOf("?")!==-1 ){ //find the query
      query = url.substr( url.indexOf("?") + 1 );
      url = url.substr(0, url.indexOf("?"));
    }    
    query += (query ? "&" : "") + "ahah_fragment=%23" + fragment + "&ahah_format=" + format;
    
    url += "?" + query;
    return url;
  }
  return url;
}